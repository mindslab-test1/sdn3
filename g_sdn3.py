#!/usr/local/bin/python
# -*- coding: utf-8 -*-
"""
테스트 실행
./sdn.py 2>&1 | tee sdn.log
"""
from concurrent import futures
import time
import datetime
import logging
import traceback
import os
import sys
import struct
import threading
from multiprocessing import Process
import grpc
import string

from maum.brain.tts import ng_tts_pb2
from maum.brain.tts import ng_tts_pb2_grpc

import sqlite3
import configparser
import datetime as dt
import argparse
import glob
import numpy as np
import io
import rubberband
import soundfile
import wave
import audioop
import soxr

import base64
import hashlib
from Cryptodome import Random
from Cryptodome.Cipher import AES
import yaml
from google.cloud import texttospeech_v1

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

LOGGER = None
DB_LOCK = threading.Lock()
MAUM_KEY = 'akdmazjsprxmdlqslek'    # 마음커넥트입니다
MAUM_IV = b'0123456789012345'
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS).encode()
unpad = lambda s : s[:-ord(s[len(s)-1:])]

class AESCipher:
    def __init__(self, key):
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = pad(raw.encode())
        #iv = Random.new().read(BS)
        cipher = AES.new(self.key, AES.MODE_CBC, MAUM_IV)
        return base64.b64encode(MAUM_IV + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:BS]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(enc[BS:])).decode('utf-8')

class MyFormatter(logging.Formatter):
    converter = dt.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


def getLogger(name, log_level):
    formatter = MyFormatter(fmt='%(asctime)s [%(levelname)5s] %(threadName)s %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.addHandler(handler)
    logger.setLevel(log_level)
    logger.propagate = False
    return logger


def clean_cache(cfg, camp_id, file_name=None):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    try:
        if file_name:
            t = (camp_id, file_name)
            c.execute("DELETE FROM TTS_CACHE WHERE camp_id = ? AND file_name = ?", t)
            conn.commit()
        else:
            t = (camp_id,)
            c.execute("DELETE FROM TTS_CACHE WHERE camp_id = ?", t)
            conn.commit()
    except sqlite3.Error as e:
        LOGGER.error('Failed to DELETE TABLE: ' + e.message)
    conn.close()

    if file_name:
        try:
            os.remove(file_name)
        except OSError as e:
            LOGGER.error('Error while deleting file {}: {}'.format(file_name, e))
    else:
        file_glob = os.path.join(cfg.cache_directory, "{}_*.wav".format(camp_id))
        remove_list = glob.glob(file_glob)
        for f in remove_list:
            try:
                os.remove(f)
            except OSError as e:
                LOGGER.error('Error while deleting file {}: {}'.format(f, e))


def create_table(cfg):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    query = '''
CREATE TABLE IF NOT EXISTS TTS_CACHE (
    seq         INTEGER PRIMARY KEY AUTOINCREMENT,
    camp_id     INTEGER,
    speaker_id  INTEGER,
    text        TEXT,
    file_name   TEXT UNIQUE,
    in_progress TEXT,
    last_used_time TEXT
)
'''
    query_idx = '''
CREATE INDEX IF NOT EXISTS TTS_CACHE_camp_id_IDX ON TTS_CACHE (camp_id, speaker_id, "text");
'''
    query_idx2 = '''
CREATE INDEX IF NOT EXISTS TTS_CACHE_last_used_time_IDX ON TTS_CACHE (last_used_time);
'''
    try:
        c.execute(query)
        c.execute(query_idx)
        c.execute(query_idx2)
        conn.commit()
    except sqlite3.Error as e:
        LOGGER.error('Failed to CREATE TABLE: ' + e.message)
    conn.close()


def wait_tts(cur, fname):
    for i in range(120):         # max 30 seconds
        cur.execute("SELECT in_progress FROM tts_cache WHERE file_name = '%s'" % fname)
        row = cur.fetchone()
        if row is None:
            return False
        elif row[0] == 'Y':
            time.sleep(1)
        else:
            return True


def complete_tts(cfg, fname):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    c.execute("UPDATE tts_cache SET in_progress = 'N' where file_name = '%s'" % fname)
    conn.commit()
    conn.close()


def find_wav(cfg, camp_id, speaker_id, text):
    encrypted = AESCipher(MAUM_KEY).encrypt(text)
    #print('encrypted: {}'.format(encrypted))
    fname = None
    new_file = ''
    in_progress = ''
    try:
        conn = sqlite3.connect(cfg.db)
        c = conn.cursor()
        with DB_LOCK:
            t = (camp_id, speaker_id, encrypted)
            c.execute("SELECT file_name, in_progress FROM tts_cache "
                      "WHERE  camp_id = ? and speaker_id = ? and text = ? and in_progress = 'N'", t)
            row = c.fetchone()
            if row is None:
                c.execute("INSERT INTO tts_cache "
                          "  (camp_id, speaker_id, text, last_used_time) "
                          "VALUES (?, ?, ?, DATETIME('now', 'localtime'))", t)
                seq = c.lastrowid
                new_file = os.path.join(cfg.cache_directory, "{}_{}.wav".format(camp_id, seq))
                c.execute("UPDATE tts_cache SET file_name = ?, in_progress = 'Y' WHERE seq = ?",
                          (new_file, seq))
                conn.commit()
                LOGGER.info('Request TTS - new wav file')
                LOGGER.info('\tcampaign_id: {}, speaker_id: {}, file: {}'.format(camp_id, speaker_id, new_file))
                LOGGER.debug('\ttext: "{}"'.format(text))
            else:
                conn.commit()
                fname = row[0]
                in_progress = row[1]
                c.execute("UPDATE tts_cache SET last_used_time = DATETIME('now', 'localtime') where file_name = ?",
                          (fname, ))
                conn.commit()
                LOGGER.info('Request TTS - found in cache')
                LOGGER.info('\tcampaign_id: {}, speaker_id: {}, file: {}'.format(camp_id, speaker_id, fname))
                LOGGER.debug('\ttext: "{}"'.format(text))

        if in_progress == 'Y':
            LOGGER.info("Wait to generate TTS from other request")
            # wait_tts(c, fname)
        conn.close()
    except Exception as e:
        LOGGER.error('find_wav() error: %s' % str(e))

    return (fname, new_file)


def remove_wav(cfg, new_file):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    c.execute("DELETE FROM tts_cache WHERE file_name = ?", (new_file,))
    conn.commit()
    conn.close()


class SdnConfig:
    def __init__(self, cfg_name = ''):
        self.model_list = dict()
        cfg_files = list()
        if len(cfg_name) > 0:
            cfg_files = [cfg_name]
        else:
            cfg_files = [os.environ['MAUM_ROOT'] + '/etc/sdn.yaml', 'sdn.yaml']

        for cfg in cfg_files:
            if os.path.exists(cfg):
                try:
                    global LOGGER
                    f = open(cfg)
                    self.config = yaml.safe_load(f)
                    if LOGGER:
                        LOGGER.info('found config file: {}'.format(cfg))
                    else:
                        self.log_level = self.config['log']['level']
                        LOGGER = getLogger('sdn', self.log_level * 10)
                    self.db = self.config['common']['db']
                    self.listen_addr = self.config['common']['listen_addr']
                    self.cache_directory = self.config['common']['cache_directory']
                    self.worker = self.config['common']['worker']
                    self.max_days = self.config['cache']['max_days']
                    self.clean_time = self.config['cache']['clean_time']
                    default_values = self.config.get('default')
                    if default_values:
                        self.codec = default_values.get('codec', 'pcm')
                    else:
                        self.codec = 'pcm'
                except yaml.parser.ParserError as e:
                    if not LOGGER:
                        LOGGER = getLogger('sdn', logging.DEBUG)
                    LOGGER.error('Config file ParseError: {}'.format(e))
                break

    def get_model_list(self):
        for tts in self.config['tts']:
            tts_id = str(tts['id'])
            # 2020-12-03 by shinwc for Kubenates integration
            tts_addr_list = []
            for ta in tts['addr']:
               ta = os.path.expandvars(ta)
               LOGGER.info('TTS[{}]: {}'.format(tts_id, ta))
               tts_addr_list.append(ta)
            #remote_list = tts['addr']
            remote_list = tts_addr_list 
            self.model_list[tts_id] = {remote: 0 for remote in remote_list}

        return self.model_list


def encode(data, codec):
    encoded = data
    if codec == 'alaw':
        encoded = audioop.lin2alaw(data, 2)
    elif codec == 'ulaw':
        encoded = audioop.lin2ulaw(data, 2)
    return encoded


class NgTtsServiceServicer(ng_tts_pb2_grpc.NgTtsServiceServicer):
    """Provides methods that implement functionality of route guide server."""

    def __init__(self):
        config = SdnConfig()
        self.model_list = config.get_model_list()
        self.lock = threading.Lock()
        # self.db = route_guide_resources.read_route_guide_database()
        self.delimiter = '|'

    def get_tts_remote(self, campaign='default'):
        self.lock.acquire()
        try:
            if campaign not in self.model_list:
                campaign = 'default'
            remote_list = self.model_list[campaign]
            addr = min(remote_list, key=remote_list.get)
            self.model_list[campaign][addr] += 1
        except Exception as e:
            LOGGER.error('Failed to get_tts_remote(): ' + e.message)
        self.lock.release()
        return addr

    def release_tts_remote(self, remote, campaign='default'):
        self.lock.acquire()
        try:
            self.model_list[campaign][remote] -= 1
        except Exception:
            pass
        self.lock.release()

    def change_wav_header(self, header, target_rate):
        new_header = header[:12]
        riff, size, fformat = struct.unpack('<4sI4s', header[:12])
        LOGGER.info("Original wav - Riff: %s, Chunk Size: %i, format: %s" % (riff, size, fformat))

        chunk_header = header[12:20]
        new_header += chunk_header
        subchunkid, subchunksize = struct.unpack('<4sI', chunk_header)

        if (subchunkid == b'fmt '):
            aformat, channels, samplerate, byterate, blockalign, bps = struct.unpack('HHIIHH', header[20:36])
            bitrate = (samplerate * channels * bps) / 1024
            LOGGER.info("Original wav - Format: %i, Channels %i, Sample Rate: %i, Kbps: %i"
                        % (aformat, channels, samplerate, bitrate))
            new_byterate = (bps / 8) * target_rate * channels
            new_header += struct.pack('HHIIHH', aformat, channels, target_rate, new_byterate, blockalign, bps)
            audio_chunk_id, audio_chunk_size = struct.unpack('<4sl', header[36:44])
            new_audio_chunk = struct.pack('<4sl', audio_chunk_id,
                                           audio_chunk_size / (float(samplerate) / target_rate))

            new_header += new_audio_chunk
            new_size = size / (float(samplerate) / target_rate)
            new_header = struct.pack('<4sI4s', riff, new_size, fformat) + new_header[12:]
        else:
            return header
        return new_header

    def gen_wav_header(self, sample_rate):
        bits_per_sample = 16
        channels = 1
        data_size = 10000000

        # 0 ~ 12
        header = struct.pack('!4sI4s', b'RIFF', data_size + 44, b'WAVE')
        # 12 ~ 20
        header += struct.pack('<4sl', b'fmt ', 16)
        # 20 ~ 36
        header += struct.pack('<HHIIHH',
                              1,    # (2byte) Format type (1 - PCM)
                              channels,
                              sample_rate,
                              sample_rate * channels * bits_per_sample // 8,
                              channels * bits_per_sample // 8,
                              bits_per_sample)
        # 36 ~ 44
        header += struct.pack('<4sl', b'data', data_size)
        return header

    def speak_wav_from_cache(self, cache_file, target_rate, tempo, codec):
        root, ext = os.path.splitext(cache_file)
        tempo_file = '{}_tempo_{}{}'.format(root, tempo, ext)
        if os.path.isfile(tempo_file):
            LOGGER.info('{} exists'.format(tempo_file))
            cache_file = tempo_file
        elif tempo > 1.00 or tempo < 1.00:
            inv_tempo = 1 / float(tempo)
            data, rate = soundfile.read(cache_file, dtype='int16')
            out = rubberband.stretch(data, rate=rate, ratio=inv_tempo, 
                    crispness=5, formants=False, precise=True)
            soundfile.write(tempo_file, out, rate, 'PCM_16')
            LOGGER.info('{} is created'.format(tempo_file))
            cache_file = tempo_file

        rf = wave.open(cache_file, 'rb')
        input_rate = rf.getframerate()
        rs = soxr.ResampleStream(
            input_rate,       # input samplerate
            target_rate,      # target samplerate
            1,                # channel(s)
            dtype='int16'    # data type (default = 'float32')
        )
        chunk_size = 8192 
        while True:
            chunk = rf.readframes(chunk_size)
            if chunk:
                raw_data = np.frombuffer(chunk, dtype=np.int16)
                #print('chunk: {} {}'.format(len(chunk), type(chunk)))
                end_input = False if len(chunk) == chunk_size*2 else True
                resampled_data = rs.resample_chunk(
                    raw_data,             # 1D(mono) or 2D(frames, channels) array input
                    last=end_input        # Set True at end of input
                )
                #print('resampled data: {} {}'.format(len(resampled_data), type(resampled_data)))
                raw_data = resampled_data.tobytes()
                tts = ng_tts_pb2.TtsMediaResponse()
                tts.mediaData = encode(raw_data, codec)
                yield tts
            else:
                break
        rf.close()
        LOGGER.info('DONE to send {}'.format(cache_file))

    def speak_wav_from_google(self, campaign, request, new_file, target_rate, tempo, use_cache, config, codec, voice_name):
        client = texttospeech_v1.TextToSpeechClient()
        synthesis_input = texttospeech_v1.SynthesisInput(text=request.text)
        voice = texttospeech_v1.VoiceSelectionParams(
            language_code="ko-KR", 
            #name="ko-KR-Wavenet-A",
            name=voice_name,
            ssml_gender=texttospeech_v1.SsmlVoiceGender.FEMALE
        )

        # Select the type of audio file you want returned
        input_rate = 24000
        audio_config = texttospeech_v1.AudioConfig(
            audio_encoding=texttospeech_v1.AudioEncoding.LINEAR16,
            sample_rate_hertz=24000
        )

        try:
            # Perform the text-to-speech request on the text input with the selected
            # voice parameters and audio file type
            response = client.synthesize_speech(
                input=synthesis_input, voice=voice, audio_config=audio_config
            )
            LOGGER.info(f'>>> received length: {len(response.audio_content)}')

            rs = soxr.ResampleStream(
                input_rate,       # input samplerate
                target_rate,      # target samplerate
                1,                # channel(s)
                dtype='int16'     # data type (default = 'float32')
            )

            raw_data = response.audio_content
            if len(raw_data) > 0:
                data = np.frombuffer(raw_data, dtype=np.int16)
                resampled_data = rs.resample_chunk(
                    data,             # 1D(mono) or 2D(frames, channels) array input
                    last=False        # Set True at end of input
                )
                raw_data = resampled_data.tobytes()
                tts = ng_tts_pb2.TtsMediaResponse()
                tts.mediaData = encode(raw_data, codec)
                yield tts
            LOGGER.info('DONE to send {}'.format(new_file))
            # The response's audio_content is binary.
            if use_cache:
                """
                wf = wave.open(new_file, 'wb')
                wf.setnchannels(1)
                wf.setsampwidth(2)
                wf.setframerate(input_rate)
                wf.writeframes(response.audio_content)
                wf.close()
                LOGGER.info('Done to write {}'.format(new_file))
                """
                with open(new_file, "wb") as out:
                    # Write the response to the output file.
                    out.write(response.audio_content)
                    complete_tts(config, new_file)
                    LOGGER.info('Done to write {}'.format(new_file))
        except grpc.RpcError as e:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            if os.path.exists(new_file):
                os.remove(new_file)
            LOGGER.error('FAIL to send: {}'.format(e))
        except GeneratorExit:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            if os.path.exists(new_file):
                os.remove(new_file)
            LOGGER.info('DONE to send (GeneratorExit): {}'.format(new_file))
        except:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            LOGGER.error('FAIL to send: unknown error')

    def speak_wav_from_server(self, campaign, request, new_file, target_rate, tempo, use_cache, config, codec):
        remote = self.get_tts_remote(campaign)
        LOGGER.info('tts remote address is {}'.format(remote))
        channel = grpc.insecure_channel(remote)
        stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)

        try:
            request.audioEncoding = ng_tts_pb2.PCM
            resp = stub.SpeakWav(request)
            LOGGER.info('start to write {}'.format(new_file))
            input_rate = 22050
            if use_cache:
                wf = wave.open(new_file, 'wb')
                wf.setnchannels(1)
                wf.setsampwidth(2)
                wf.setframerate(input_rate)

            rs = soxr.ResampleStream(
                input_rate,       # input samplerate
                target_rate,      # target samplerate
                1,                # channel(s)
                dtype='int16'     # data type (default = 'float32')
            )
            received = b''
            chunk_size = 8192 * 4
            inv_tempo = 1 / float(tempo)
            for tts in resp:
                if tts.mediaData.startswith(b'RIFF'):
                    continue
                if use_cache:
                    wf.writeframes(tts.mediaData)
                received += tts.mediaData
                if len(received) >= chunk_size:
                    tts.mediaData = received[:chunk_size]
                    received = received[chunk_size:]
                else:
                    continue

                if tempo > 1.00 or tempo < 1.00:
                    chunk = np.frombuffer(tts.mediaData, dtype=np.int16)
                    #print(f'received length: {len(tts.mediaData)}, chunk length: {len(chunk)}')
                    raw_data = rubberband.stretch(chunk, rate=input_rate, ratio=inv_tempo, crispness=5, formants=False, precise=True)
                    #print(f'tempo: {tempo}, stretched data length: {len(raw_data)}')
                else:
                    raw_data = tts.mediaData

                if len(raw_data) > 0:
                    data = np.frombuffer(raw_data, dtype=np.int16)
                    resampled_data = rs.resample_chunk(
                        data,             # 1D(mono) or 2D(frames, channels) array input
                        last=False        # Set True at end of input
                    )
                    raw_data = resampled_data.tobytes()
                    tts = ng_tts_pb2.TtsMediaResponse()
                    tts.mediaData = encode(raw_data, codec)
                    yield tts

            if len(received) > 0:
                if tempo > 1.00 or tempo < 1.00:
                    chunk = np.frombuffer(received, dtype=np.int16)
                    #print(f'received length: {len(received)}, chunk length: {len(chunk)}')
                    raw_data = rubberband.stretch(chunk, rate=input_rate, ratio=inv_tempo, crispness=5, formants=False, precise=True)
                    #print(f'tempo: {tempo}, stretched data length: {len(raw_data)}')
                else:
                    raw_data = received

                if len(raw_data) > 0:
                    data = np.frombuffer(raw_data, dtype=np.int16)
                    resampled_data = rs.resample_chunk(
                        data,             # 1D(mono) or 2D(frames, channels) array input
                        last=True        # Set True at end of input
                    )
                    raw_data = resampled_data.tobytes()
                    tts = ng_tts_pb2.TtsMediaResponse()
                    tts.mediaData = encode(raw_data, codec)
                    yield tts

            if use_cache:
                wf.close()
                complete_tts(config, new_file)
            LOGGER.info('DONE to send {}'.format(new_file))
        except grpc.RpcError as e:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            if os.path.exists(new_file):
                os.remove(new_file)
            LOGGER.error('FAIL to send: {}'.format(e))
        except GeneratorExit:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            if os.path.exists(new_file):
                os.remove(new_file)
            LOGGER.info('DONE to send (GeneratorExit): {}'.format(new_file))
        except:
            complete_tts(config, new_file)
            remove_wav(config, new_file)
            LOGGER.error('FAIL to send: unknown error')
        self.release_tts_remote(remote, campaign)

    def SpeakWav(self, request, context):
        config = SdnConfig()
        request.text = request.text.strip()
        tempo = 1.0
        campaign = 'default'
        target_rate = 16000
        use_cache = True
        codec = config.codec
        for k, v in context.invocation_metadata():
            if k == 'tempo':
                tempo = float(v)
            elif k == 'campaign':
                campaign = v
            elif k == 'voice':
                voice_name = v
            elif k == 'samplerate':
                target_rate = int(v)
            elif k == 'codec':
                # pcm or alaw or ulaw
                codec = v
            elif k == 'use_cache':
                if v == "false":
                    use_cache = False

        line_list = request.text.split(self.delimiter)
        if request.audioEncoding == ng_tts_pb2.WAV:
            tts = ng_tts_pb2.TtsMediaResponse()
            tts.mediaData = self.gen_wav_header(target_rate)
            yield tts

        for line in line_list:
            line = line.strip()
            if len(line) <= 0:
                continue
            LOGGER.info('Partial text: {}'.format(line))
            sub_request = ng_tts_pb2.TtsRequest()
            sub_request.lang = request.lang
            sub_request.sampleRate = request.sampleRate
            sub_request.speaker = request.speaker
            sub_request.text = line

            cache_file = None
            new_file = ''
            if use_cache:
                cache_file, new_file = find_wav(config, campaign, sub_request.speaker, sub_request.text)
            #print('cache_file: {}, new_file: {}'.format(cache_file, new_file))
            if cache_file:
                for resp in self.speak_wav_from_cache(cache_file, target_rate, tempo, codec):
                    yield resp
            else:
            #    for resp in self.speak_wav_from_server(campaign, sub_request, new_file, target_rate, tempo, use_cache, config, codec):
                for resp in self.speak_wav_from_google(campaign, sub_request, new_file, target_rate, tempo, use_cache, config, codec, voice_name):
                    yield resp


def clean_old_cache(cfg):
    while True:
        now = datetime.datetime.today()
        reserved_time = datetime.datetime(now.year, now.month, now.day, int(cfg.clean_time), 0)
        if now.hour >= int(cfg.clean_time):
            reserved_time += datetime.timedelta(days=1)
        LOGGER.info('Next time to remove cache is {}'.format(reserved_time))
        time.sleep((reserved_time-now).total_seconds())

        try:
            LOGGER.info('Start cleaning cache...')
            conn = sqlite3.connect(cfg.db)
            cur = conn.cursor()

            sql = '''
                SELECT
                    seq, camp_id, file_name
                FROM
                    TTS_CACHE
                WHERE
                    last_used_time < DATETIME('now', '-{} days', 'localtime');'''.format(cfg.max_days)

            cur.execute(sql)
            rows = cur.fetchall()
            for row in rows:
                LOGGER.info('delete camp_id:{}, file: {}'.format(row[1], row[2]))
                clean_cache(cfg, row[1], row[2])
            conn.close()
            LOGGER.info('End of cleaning cache...')

        except Exception as e:
            LOGGER.error(traceback.format_exc())


def serve(cfg):
    create_table(cfg)

    p = Process(target=clean_old_cache, args=(cfg,))
    p.daemon = True
    p.start()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=int(cfg.worker)))
    ng_tts_pb2_grpc.add_NgTtsServiceServicer_to_server(
        NgTtsServiceServicer(), server)
    if server.add_insecure_port(cfg.listen_addr) <= 0:
        sys.exit(1)

    server.start()
    try:
        LOGGER.info("SDN (Speech Delivery Network) GRPC Server started...")
        LOGGER.info("sqlite3 db path     : {}".format(cfg.db))
        LOGGER.info("grpc listen addr    : {}".format(cfg.listen_addr))
        LOGGER.info("default audio codec : {}".format(cfg.codec))
        LOGGER.info("wav cache directory : {}".format(cfg.cache_directory))
        LOGGER.info("cache max days      : {}".format(cfg.max_days))
        LOGGER.info("cache clean time    : {}".format(cfg.clean_time))

        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    config = SdnConfig()
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clean', nargs='?', const=-1, type=int, metavar='CampaignID',
                        help='clean cache with campaign id')
    args = parser.parse_args()
#    if args.clean >= 0:
    if args.clean != None:
        clean_cache(config, args.clean)
    else:
        serve(config)
