# SDN3 development
SDN3 개발 항목 
- python 2.7의 의존성을 없애고 python 3.8 이상의 버전을 지원 
- tempo 기능 업그레이드
- 현재 tempo(재생 speed) 조정은 pysoundtouch library를 이용하여 time stretch 기능을 제공
- 보통 0.8~1.2배 정도의 제한적인 범위 내의 재생 속도는 적절히 지원하는 것으로 판단됨
- 1.3배 이상의 tempo를 조정할 경우 '에코'가 포함되어 음질에 문제가 생김
- pysoundtouch보다 인기(github의 스타 수)있는 librosa에서도 동일한 문제가 나타나 것으로 판단됨

## Time stretch 기능 검토
Time stretch 기능을 제공하는 python 오픈 소스 라이브러리는 크게 3가지 정도인데 [Stackoverflow 링크](https://stackoverflow.com/questions/59867659/python-time-stretch-wave-files-comparison-between-three-methods) 비교를 참고해서 Rubber Band time stretch 기능을 사용하는 것으로 확정

### librosa 라이브러리
---
> librosa.effects.time_stretch(signal, speed) (docs)

> In essence, this approach transforms the signal using stft (short time Fourier transform), stretches it using a phase vocoder and uses the inverse stft to reconstruct the time domain signal. Typically, when doing it this way, one introduces a little bit of "phasiness", i.e. a metallic clang, because the phase cannot be reconstructed 100%. That's probably what you've identified as "echo."

> Use a better time_stretch library, e.g. rubberband. librosa is great, but its current time scale modification (TSM) algorithm is not state of the art. For a review of TSM algorithms, see for example this article.

librosa의 time strech 기능은 최신 알고리즘이 아니라 rubberband를 사용할 것을 제안. 가장 현실적인 방안인데 python 환경이 달라지면서 개발환경 구성하는 것에 노력이 필요할 것으로 보임(2.7 -> 3.6 이상) 

### Rubber Band와 SoundTouch
---
SoundTouch와 다르게 Rubber Band 라이브러리는 주파수 도메인에서 작동하기 때문에 audio quality가 보장되지만 CPU load가 아주 높아서 모바일에서 실시간 처리는 어렵고 서버 환경에서도 부하 때문에 복수 개의 processing 시에 문제가 발생할 가능성이 높음

Time stretch된 audio를 caching하는 것도 하나의 방법

[링크 참고](https://superpowered.com/free-open-source-time-stretching-pitch-shifting)

> Rubber Band Library
>>  This is a big library with lots and lots of code inside. The audio quality is great, as the author did a solid job researching audio algorithms. It handles audio transients well (not losing them) and does a great job in preserving audio quality (reducing so-called “phasiness”).

>>  But the digital signal processing work is not stellar, Rubber Band’s CPU load is so high that you cannot run it on a mobile device for real-time processing, even if you try to utilize every DSP hack and trick you might know.

> SoundTouch Audio Processing
>>  A nice, compact library with an acceptable CPU load, it can work real-time on a mobile device. But SoundTouch’s time-stretching works in the time domain.

>>  There are basically two kinds of time-stretching methods: time-domain and frequency domain. The aforementioned Rubber Band (and all quality commercial audio libraries) work in the frequency domain for highest audio quality.

>>  Time-domain stretching works with overlapping windows, providing absolutely no “phasiness” with the cost of missing or doubling some parts of the audio, which sounds strange. And, even if you handle transients somehow, the overall result sounds too “compressed”.

>>  SoundTouch does not handle transients, so it doesn’t work for most modern music, unfortunately. A prominent unwanted audio artifact is oddness with drum kicks. It either misses them completely or doubles them.

## Python 3.8 개발 환경 도커 빌드 
- python3.8-buster를 이용해서 python docker 빌드
- sdn 소스를 copy해서 docker 내에 생성
- requirements.txt를 확인해서 python package install
- rubberband 설치: 설치하기 전에 libsndfile과 librubberband를 설치해야 함, 설치하고 에러 발생하여 [stackoverflow](https://stackoverflow.com/questions/59899103/python-3-6-how-to-install-rubberband)를 참고하여 해결

### Python 설치 패키지 
---
파이썬 패키지를 설치하기 위해서 아래와 같이 의존성 없는(다른 패키지에서 required하지 않는)를 확인하여 requirements.txt를 만듬
+ requirements.txt.gtts: Google TTS API 설치를 위한 google-cloud-texttospeech 패키지 포함
+ requirements.txt.original: google-cloud-texttospeech 패키지 제외 
```
root@452700dcdbc3:~/sdn3# pip list --not-required
Package        Version
-------------- ------------
configparser   5.0.2
futures        3.1.1
grpcio-tools   1.38.1
pip-autoremove 0.10.0
pycryptodome   3.10.1
pycryptodomex  3.10.1
PyYAML         5.4.1
rubberband     1.0.2
shellescape    3.8.1
SoundFile      0.10.3.post1
soxr           0.2.5
supervisor     4.2.2
wheel          0.36.2
```
+ rubberband 패키지는 아래와 같이 따로 설치해야 함
```
python3 -m pip download rubberband
tar zxvf rubberband-1.0.2.tar.gz 
cd rubberband-1.0.2
```
Edit `rubberband-1.0.2/src/numpy.cpp` 
- add `#include <algorithm>` at the top of the file.
```
python3 -m pip install .
```

## 개발 관련 수정한 부분 정리
### pycrypto 라이브러리 교체
---
+ 기존 코드가 AES encript에서 멈추고 진행되지 않음
+ pycrypto library는 2.7 기반으로 3.6 이상에선 pycryptodome이나 pycryptodomex를 설치해야 함
+ 기존 설치된 pycryto 라이브러리와 충돌 문제로 pycrpytodomex를 설치하고 아래와 같이 코드에서 import함
```
pip install pycryptodomex
```
```python
from Cryptodome import Random
from Cryptodome.Cipher import AES
```
