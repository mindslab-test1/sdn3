SDN := sdn3

all: pb $(SDN)

pb:
	make -C proto install

$(SDN):
	pyinstaller --onefile sdn3.py

install: all
	@cp dist/$(SDN) $(MAUM_ROOT)/bin

clean:
	@make -C proto clean
