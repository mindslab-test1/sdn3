#!/usr/bin/python

import unittest
import sys
import samplerate 
import numpy as np

import io
import rubberband
import soundfile
import librosa
import time

def test_resample():
    converter = 'sinc_best'  # or 'sinc_fastest', ...
    resampler = samplerate.Resampler(converter, channels=1)

    f = open('1_3.wav', 'rb')
    outf = open('resampled.pcm', 'wb')
    wav_binary = f.read(44)
    wav_header = io.BytesIO(wav_binary)
    print(f'Wav header type is : {type(wav_header)}')
    sr = librosa.get_samplerate(wav_header)
    print('input_rate is {}'.format(sr))

    chunksize = 8192 * 16 
    new_rate = 8000 / float(sr)
    while True:
        buff = f.read(chunksize)
        if buff:
            chunk = np.frombuffer(buff, dtype=np.int16)
            n_frames = len(chunk)
            duration_old = n_frames/sr
            duration_new = 0.7 * duration_old
            print('duration_old: {}, duration_new: {}'.format(duration_old, duration_new))
            ratio = duration_new/duration_old
            print('n_frames: {}, ratio: {}'.format(n_frames, ratio))
            raw_data = rubberband.stretch(chunk,rate=sr,ratio=ratio,crispness=6,formants=False,precise=True)
            print('raw_data: {}'.format(len(raw_data)))
            if len(raw_data) > 0:
                data = np.frombuffer(raw_data, dtype=np.int16)
                #outf.write(data.tostring())
                end_input = False if n_frames*2 == chunksize else True
                print('end_of_input: {} {} {}'.format(end_input, n_frames, chunksize))
                resampled_data = resampler.process(data, new_rate, end_of_input=end_input).astype('int16')
                #resampled_data = resampler.process(data, new_rate).astype('int16')
                print('resampled data: {}:{}, data: {}:{}'.format(len(resampled_data), resampled_data.dtype, len(data), data.dtype))
                outf.write(resampled_data.tostring())
            """
            if len(raw_data) > 0:
                print(f'raw_data type is : {type(raw_data)}')
                #y, sr = librosa.load(raw_data, sr)
                y = np.frombuffer(raw_data, dtype=np.int16)
                print(f'frombuffer to : {type(y)}')
                data = librosa.resample(y, sr, 8000)
                print('resampled data: {}'.format(len(data)))
                outf.write(data)
            # raw_data = chunk
            if len(raw_data) > 0:
                data = np.fromstring(raw_data, dtype=np.int16)
                resampled_data = resampler.process(data, new_rate).astype('int16')
                new_data = resampled_data.tostring()
                print(len(resampled_data), len(new_data), resampled_data.dtype)
                outf.write(resampled_data.tostring())
            """
        else:
            break

    outf.close()
    f.close()

def rubberband_stretch():
    data,rate=soundfile.read('1_3.wav',dtype='int16')
    bitrate=rate*16
    print('bitrate: {}'.format(bitrate))
    nFrames=len(data)
    print('nFrames: {}'.format(nFrames))
    print(f'Raw input type is : {type(data)}')

    oldDuration=nFrames/rate
    newDuration=33.376
    print('Old duration: {}, New duration: {}'.format(oldDuration, newDuration))
    ratio=newDuration/oldDuration
    print('ratio: {}'.format(ratio))

    out=rubberband.stretch(data,rate=rate,ratio=ratio,crispness=5,formants=False,precise=True)
    soundfile.write('outfile.wav',out,rate,'PCM_16')

def stretch_and_resample():
    start = time.time()
    data, rate = soundfile.read('1_3.wav',dtype='int16')
    out = rubberband.stretch(data, rate=rate, ratio=0.75, crispness=1, formants=False, precise=True)
    end = time.time()
    soundfile.write('1_3_out.wav', out, rate, 'PCM_16')
    print('Rubber Band time stretch took {} seconds'.format(end - start))

    f = open('1_3_out.wav', 'rb')
    outf = open('resampled.pcm', 'wb')
    wav_binary = f.read(44)
    wav_header = io.BytesIO(wav_binary)
    print(f'Wav header type is : {type(wav_header)}')
    sr = librosa.get_samplerate(wav_header)
    print('input_rate is {}'.format(sr))

    converter = 'sinc_best'  # or 'sinc_fastest', ...
    resampler = samplerate.Resampler(converter, channels=1)

    chunksize = 8192 * 8 
    new_rate = 8000 / float(sr)

    while True:
        buff = f.read(chunksize)
        if buff:
            raw_data = np.frombuffer(buff, dtype=np.int16)
            #print('raw_data: {}'.format(len(raw_data)))
            n_frames = len(raw_data)
            if len(raw_data) > 0:
                data = np.frombuffer(raw_data, dtype=np.int16)
                end_input = False if n_frames*2 == chunksize else True
                #print('end_of_input: {} {} {}'.format(end_input, n_frames, chunksize))
                resampled_data = resampler.process(data, new_rate, end_of_input=end_input).astype('int16')
                print('resampled data: {}:{}, data: {}:{}'.format(len(resampled_data), resampled_data.dtype, len(data), data.dtype))
                outf.write(resampled_data.tobytes())
        else:
            break

    outf.close()
    f.close()

if __name__ == '__main__':
    #unittest.main()
    #rubberband_stretch()
    stretch_and_resample()
