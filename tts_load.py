#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import grpc
import getopt
import logging
from maum.brain.tts import ng_tts_pb2
from maum.brain.tts import ng_tts_pb2_grpc
from maum.common import lang_pb2
import multiprocessing
from multiprocessing import Process
from multiprocessing import Pool
import os
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2
from grpc_health.v1 import health_pb2_grpc
import uuid

# For your reference
SDN = '172.17.0.1:50053'
TTS = '172.17.0.1:9999'

LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s - %(message)s"

logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("tts_load")
logger.setLevel(logging.INFO)

def usage():
    print('Usage: {} [-h host:port] textfile'.format(sys.argv[0]))

def text_from_file(filename):
    with open(filename, "r") as f:         
        for line in f.readlines():
            li=line.lstrip()
            if not li.startswith("#"):
                tts_text = li 
        return tts_text

# server의 status를 return하는 method
def healthCheckStatus(stub, request):
    try:
        result = stub.Check(request)
        return result.status
    except grpc.RpcError as e:
        return 2

def unittask(index):
    if os.path.exists('./key/server.crt'):
        with open('./key/server.crt', 'rb') as f:
            root_cert = f.read()
        channel_credentials = grpc.ssl_channel_credentials(root_cert)
        channel = grpc.secure_channel(host, channel_credentials, options=(('grpc.ssl_target_name_override','mindslab'),))
    else:
        channel = grpc.insecure_channel(host)

    #add health check
    stub = health_pb2_grpc.HealthStub(channel)
    request = health_pb2.HealthCheckRequest()
    status = healthCheckStatus(stub, request)
    if status != 1: #status가 1이면 정상
        print("health checking error!! : ", status)
        return
    print("health checking : ", status)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    req.text = text_from_file(filename)
    req.speaker = 0
    logger.info('Task[{}] req.text: {}'.format(index, req.text))
    session_id = uuid.uuid1()
    st = time.time()
    try:
        #metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # metadata={(b'tempo', b'1.4'), (b'campaign', b'1'), (b'samplerate', b'16000'), (b'codec', b'pcm')}
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1'), (b'samplerate', b'16000'), 
            (b'codec', b'pcm'), (b'account', b'KBCARD'), (b'session', f'{session_id}')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        f = open('/fsapplog/test%03d.wav' % index, 'wb')
        remained = 0 
        cnt = 0
        for tts in resp:
            f.write(tts.mediaData)
            remained += len(tts.mediaData)
            if (remained >= 22094): 
                cnt = cnt + 1
                if (cnt == 1):
                    logger.info('TTS task[{}] 1st media at: {}'.format(index, time.time() -st))
                remained = 0
        f.close()
    except grpc.RpcError as e:
        print(e)
    logger.info('Task[{}] total time: {}'.format(index, (time.time() - st)))

def test(index):
    channel = grpc.insecure_channel(host)
    stub = ng_tts_pb2_grpc.NgTtsServiceStub(channel)
    req = ng_tts_pb2.TtsRequest()
    req.lang = lang_pb2.kor
    req.sampleRate = sample_rate
    #req.text = TTS
    req.text = text_from_file(filename)
    req.speaker = 0
    logger.info('req.text is %s' % req.text)

    st = time.time()
    try:
        metadata={(b'tempo', b'1.0'), (b'campaign', b'1')}
        # resp = stub.SpeakWav(req, timeout=60)
        resp = stub.SpeakWav(req, metadata=metadata)
        logger.info('[%03d ] start read wav file' % index)
        f = open('test%02d.wav' % index, 'w')
        for tts in resp:
            #logger.debug('write file')
            f.write(tts.mediaData)
    except grpc.RpcError as e:
        print(e)
    logger.info('Task[{}] total time: {}'.format(index, time.time() - st))
    f.close()

def main():
    cmd = input('Select the number of concurrent tasks: ')
    concurrent_cnt = int(cmd)

    logger.info('Start...')
    st = time.time()

    num_cores = multiprocessing.cpu_count()
    logger.info('# fo CPU cores: %d' % num_cores)
    pool = Pool(num_cores)
    pool_outputs = pool.map(unittask, range(concurrent_cnt))
    #pool_outputs = pool.map(test, range(concurrent_cnt))
    pool.close()
    pool.join()
    logger.info('End time is {}'.format(time.time() - st))

if __name__ == '__main__':
    host = '172.17.0.1:50053'
    sample_rate = 16000

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h:', [])
        for option, value in opts:
            if option == '-h':
                host = value

        if len(args) < 1:
            usage()
            sys.exit(2)
        filename = args[0]

    except getopt.GetoptError as err:
        # print help information and exit:
        logger.error(str(err)) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    #main()
    unittask(0)

